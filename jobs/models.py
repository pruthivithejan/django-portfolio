from django.db import models

# Create your models here.


class Job(models.Model):
    # Title
    title = models.CharField(max_length=256)
    # Image
    image = models.ImageField(upload_to='images/')
    # Summary
    summary = models.CharField(max_length=512)

    def __str__(self):
        return self.title
